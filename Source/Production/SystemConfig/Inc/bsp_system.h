/**
 ******************************************************************************
 * @file    bsp_system.h
 * @author  leonardo
 * @version v1.0
 * @date    06 de out de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_SYSTEMCONFIG_INC_BSPSYSTEM_H
#define SOURCE_PRODUCTION_SYSTEMCONFIG_INC_BSPSYSTEM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

/**
 * @brief   Configuration of system components (CPU clock, memory, qspi, ...)
 * @param   useQSPI
 * @return  None
 */
extern void bspConfigSystem(uint8_t  useQSPI);

#ifdef __cplusplus
}
#endif

#endif // SOURCE_PRODUCTION_SYSTEMCONFIG_INC_BSPSYSTEM_H
