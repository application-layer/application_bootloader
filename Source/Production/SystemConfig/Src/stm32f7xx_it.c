/**
 ******************************************************************************
 * @file    stm32f7xx_it_Agres.c
 * @author  leonardo.pereira
 * @version V1.0.0
 * @date    9 de jun de 2017
 * @brief   Main Interrupt Service Routines.
 *          This file provides strong declarations for all custom exceptions
 * 			 handler and peripherals interrupt service routine.
 ******************************************************************************
 */

/******************************************************************************
 *            								Includes
 ******************************************************************************/

#include "SystemConfig/Inc/stm32f7xx_it.h"

#include <Production/hal_include_handler.h>
#include INCLUDE_HAL(hal.h)
#include INCLUDE_HAL(hal_cortex.h)

#if EW_USE_FREE_RTOS == 1
#include <Production/RTOS/FreeRTOS/CMSIS_RTOS/cmsis_os.h>
#endif

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/**
 * @brief
 */
QSPI_HandleTypeDef QSPIHandle;

/**
 * @brief
 */
SD_HandleTypeDef uSdHandle;

/**
 * @brief
 */
HCD_HandleTypeDef hhcd;

/**
 * @brief
 */
volatile unsigned long ulHighFrequencyTimerTicks = 0;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/******************************************************************************
 *            Cortex-M7 Processor Exceptions Handlers
 ******************************************************************************/

/**
 * @brief  This function handles Hard Fault exception.
 */
void HardFault_Handler(void)
{
   while (1);   // Go to infinite loop when Hard Fault exception occurs
}

/**
 * @brief  This function handles Memory Manage exception.
 */
void MemManage_Handler(void)
{
   while (1);   // Go to infinite loop when Memory Manage exception occurs
}

/**
 * @brief  This function handles Bus Fault exception.
 */
void BusFault_Handler(void)
{
   while (1);   // Go to infinite loop when Bus Fault exception occurs
}

/**
 * @brief  This function handles Usage Fault exception.
 */
void UsageFault_Handler(void)
{
   while (1);   // Go to infinite loop when Usage Fault exception occurs
}

#if EW_USE_FREE_RTOS == 0
/**
 * @brief  This function handles SVCall exception.
 */
void SVC_Handler(void)
{
}
#endif

#if EW_USE_FREE_RTOS == 0
/**
 * @brief  This function handles PendSVC exception.
 */
void PendSV_Handler(void)
{
}
#endif

/**
 * @brief  This function handles SysTick Handler.
 */
void SysTick_Handler(void)
{
   HAL_IncTick();

   HAL_SYSTICK_IRQHandler();

#if EW_USE_FREE_RTOS == 1
   osSystickHandler();
#endif
}

/**
 * @brief Handles SDMMC1 DMA Rx transfer interrupt request.
 */
void BSP_SDMMC1_DMA_Rx_IRQHandler(void)
{
   HAL_DMA_IRQHandler(uSdHandle.hdmarx);
}

/**
 * @brief Handles SD1 card interrupt request.
 */
void BSP_SDMMC1_IRQHandler(void)
{
   HAL_SD_IRQHandler(&uSdHandle);
}

/**
 * @brief  This function handles QUADSPI interrupt request.
 */
void QUADSPI_IRQHandler(void)
{
   HAL_QSPI_IRQHandler(&QSPIHandle);
}

/**
 * @brief  This function handles QUADSPI DMA interrupt request.
 */
void QSPI_DMA_IRQ_HANDLER(void)
{
   HAL_DMA_IRQHandler(QSPIHandle.hdma);
}

/**
 * @brief  This function handles USB-On-The-Go FS/HS global interrupt request.
 */
#ifdef USE_USB_FS
void OTG_FS_IRQHandler(void)
#else
void OTG_HS_IRQHandler(void)
#endif
{
   HAL_HCD_IRQHandler(&hhcd);
}

/**
 * @brief
 */
void TIM7_IRQHandler(void)
{
   TIM_HandleTypeDef s_TimerInstance = { .Instance = TIM7 };

   NVIC_ClearPendingIRQ(TIM7_IRQn);

   TIM_GET_CLEAR_IT(&s_TimerInstance, TIM_IT_UPDATE);

   ////////////////////////////////////////////////////////////
   /// Increment the counter used to measure execution time ///
   ////////////////////////////////////////////////////////////

   ulHighFrequencyTimerTicks++;
}
