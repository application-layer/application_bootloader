/**
 ******************************************************************************
 * @file    bsp_system.c
 * @author  leonardo
 * @version v1.0
 * @date    06 de out de 2019
 * @brief
 ******************************************************************************
 */

#include <string.h>

#include "SystemConfig/Inc/bsp_system.h"

#include <Production/bsp_include_handler.h>
#include INCLUDE_BSP(qspi.h)
#include INCLUDE_BSP(sdram.h)
#include INCLUDE_BSP(sd.h)

/**
 * @brief   System Clock Configuration
*           The system Clock is configured as follow :
*               System Clock source            = PLL (HSE)
*               SYSCLK(Hz)                     = 200000000
*               HCLK(Hz)                       = 200000000
*               AHB Prescaler                  = 1
*               APB1 Prescaler                 = 4
*               APB2 Prescaler                 = 2
*               HSE Frequency(Hz)              = 25000000
*               PLL_M                          = 25
*               PLL_N                          = 400
*               PLL_P                          = 2
*               PLL_Q                          = 8
*               PLL_R                          = 7
*               VDD(V)                         = 3.3
*               Main regulator output voltage  = Scale1 mode
*               Flash Latency(WS)              = 6
 */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef        RCC_ClkInitStruct;
  RCC_OscInitTypeDef        RCC_OscInitStruct;
  RCC_PeriphCLKInitTypeDef  PeriphClkInitStruct;

  // Enable Power Control clock
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is
     clocked below the maximum system frequency, to update the voltage scaling value
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  // Enable HSE Oscillator and activate PLL with HSE as source
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState       = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState   = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource  = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM       = 25;
  RCC_OscInitStruct.PLL.PLLN       = 432;
  RCC_OscInitStruct.PLL.PLLP       = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ       = 9;
  RCC_OscInitStruct.PLL.PLLR       = 7;

  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  // Activate the Over-Drive mode
  HAL_PWREx_EnableOverDrive();

  // Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers
  RCC_ClkInitStruct.ClockType      = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource   = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider  = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);

  // LCD clock configuration
  memset(&PeriphClkInitStruct,
          0,
          sizeof(PeriphClkInitStruct));

  HAL_RCCEx_GetPeriphCLKConfig(&PeriphClkInitStruct);

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC;

  /* PLLSAI_VCO Input = HSE_VALUE/PLL_M = 1 MHz */
  /* PLLSAI_VCO Output = PLLSAI_VCO Input * PLLSAIN = 417 MHz */
  /* PLLLCDCLK = PLLSAI_VCO Output/PLLSAIR = 417 MHz / 5 = 83.4 MHz */
  /* LTDC clock frequency = PLLLCDCLK / LTDC_PLLSAI_DIVR_2 = 83.4 / 2 = 41.7 MHz */
  PeriphClkInitStruct.PLLSAI.PLLSAIN = 417;
  PeriphClkInitStruct.PLLSAI.PLLSAIR = 5;

  PeriphClkInitStruct.PLLSAIDivR     = RCC_PLLSAIDIVR_2;

  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct);
}

/**
 * #brief   Configure the MPU attributes as Write Through for SRAM1/2.
 */
static void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct;

  /* Disable the MPU */
  HAL_MPU_Disable();

  /* Configure the MPU attributes as WT for SRAM */
  MPU_InitStruct.Enable           = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress      = 0x20020000;
  MPU_InitStruct.Size             = MPU_REGION_SIZE_512KB;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable     = MPU_ACCESS_NOT_BUFFERABLE;
  MPU_InitStruct.IsCacheable      = MPU_ACCESS_CACHEABLE;
  MPU_InitStruct.IsShareable      = MPU_ACCESS_SHAREABLE;
  MPU_InitStruct.Number           = MPU_REGION_NUMBER0;
  MPU_InitStruct.TypeExtField     = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec      = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

  /* Enable the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

void bspConfigSystem(uint8_t useQSPI)
{
    // Configure the system clock
    SystemClock_Config();

    if(useQSPI)
    {
        BSP_QSPI_Init();
        BSP_QSPI_EnableMemoryMappedMode();

        // configure QSPI: LPTR register with the low-power time out value
        WRITE_REG(QUADSPI->LPTR, 0xFFF);
    }

    BSP_SDRAM_Init();

    MPU_Config();

    // Enable I-Cache
    SCB_EnableICache();

    // Enable D-Cache
    SCB_EnableDCache();

    // Reset of all peripherals, Initializes the Flash interface and the Systick
    HAL_Init();

    BSP_SD_Init();
}
