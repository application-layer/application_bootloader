/**
 *  @file    main.cpp
 *  @author: leonardo.pereira
 *  @version v1.0
 *  @date    14 de jun de 2017
 *  @brief
 */

#include "SystemConfig/Inc/bsp_system.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

#define USE_QSPI    1

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private Function Prototypes -----------------------------------------------*/

/* Private Functions Declaration ---------------------------------------------*/

/**
 * @brief  The main function of the whole application. The main function initializes all
 *         necessary drivers and provides a for-ever loop to drive the EmWi application.
 * @retval err : Error status (0=> success, 1=> fail)
 */
auto main(void) -> int
{
    bspConfigSystem(USE_QSPI);

    while(1)
    {
        /* USER CODE END WHILE */

        /* USER CODE BEGIN */
    }

    return 0;
}
