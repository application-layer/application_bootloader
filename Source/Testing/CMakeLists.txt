#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: application_bootloader                                              #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

coloredMessage(BoldYellow "Loading Testing Source CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

enable_testing()

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/
                    ${CMAKE_CURRENT_SOURCE_DIR}/../
                    ${CMAKE_CURRENT_SOURCE_DIR}/../Production/)

#############################################
#   A.1. Set Options                        #
#############################################

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

if(${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME})
    add_subdirectory(${PROJECT_UNIT_TESTING_SOURCE_FOLDER})
elseif(${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_STUBS_BUILD_NAME})
    add_subdirectory(${PROJECT_STUB_TESTING_SOURCE_FOLDER})
endif()

#############################################
# D. Libraries / Executables                #
#############################################
