#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: application_bootloader                                              #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

set(CMAKE_C_COMPILER                    /opt/Atollic_TrueSTUDIO_for_STM32_x86_64_9.3.0/ARMTools/bin/arm-atollic-eabi-gcc)
set(CMAKE_CXX_COMPILER                  /opt/Atollic_TrueSTUDIO_for_STM32_x86_64_9.3.0/ARMTools/bin/arm-atollic-eabi-g++)
set(CMAKE_ASM_COMPILER                  /opt/Atollic_TrueSTUDIO_for_STM32_x86_64_9.3.0/ARMTools/bin/arm-atollic-eabi-gcc)
set(CROSS_TARGET_TRIPLET                /opt/Atollic_TrueSTUDIO_for_STM32_x86_64_9.3.0/ARMTools/bin/arm-atollic-eabi-objcopy)

set(COMPILER_FLAGS_GCC_VERSION      "-std=gnu11")
set(COMPILER_FLAGS_G++_VERSION      "-std=gnu++14")

set(COMPILER_FLAGS_TARGET           "-mthumb -mcpu=cortex-m7 -mfloat-abi=hard -mfpu=fpv5-d16")

set(COMPILER_FLAGS_GENERAL          "-ffunction-sections -fdata-sections -fstack-usage -mlong-calls -fPIC")
set(COMPILER_FLAGS_G++_GENERAL      "-fno-rtti -fno-exceptions -fno-threadsafe-statics -D_GLIBCXX11_USE_C99_STDIO=1 -D_GLIBCXX11_USE_C99_STDLIB=1")
set(COMPILER_FLAGS_ASM_GENERAL      "-c -x assembler-with-cpp --specs=nano.specs")
set(COMPILER_FLAGS_LINKER_GENERAL   "-T ${LINKER_SCRIPT} --specs=nosys.specs -Wl,-Map='${APP_NAME}.map' -Wl,--gc-sections -static --specs=nano.specs -Wl,--start-group -Wl,--end-group")

if(${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_RELEASE_BUILD_NAME})
    set(COMPILER_FLAGS_OPT_DEBUGGING        "-O2")
else()
    set(COMPILER_FLAGS_OPT_DEBUGGING        "-O2 -g3")
endif()

set(COMPILER_FLAGS_WARNING          "")

set(COMPILER_FLAGS_EXTERNAL_DEFINES ${EXTERNAL_DEFINES})

set(COMPILER_PROJECT_CONFIGS        ${PROJECT_CONFIG_DEFINES})

set(CMAKE_C_FLAGS           "${CMAKE_C_FLAGS}   ${COMPILER_FLAGS_TARGET} ${COMPILER_FLAGS_GCC_VERSION} ${COMPILER_FLAGS_OPT_DEBUGGING} ${COMPILER_FLAGS_GENERAL} ${COMPILER_FLAGS_WARNING} ${COMPILER_PROJECT_CONFIGS} ${COMPILER_FLAGS_EXTERNAL_DEFINES}")
set(CMAKE_CXX_FLAGS         "${CMAKE_CXX_FLAGS} ${COMPILER_FLAGS_TARGET} ${COMPILER_FLAGS_G++_VERSION} ${COMPILER_FLAGS_OPT_DEBUGGING} ${COMPILER_FLAGS_GENERAL} ${COMPILER_FLAGS_WARNING} ${COMPILER_PROJECT_CONFIGS} ${COMPILER_FLAGS_EXTERNAL_DEFINES} ${COMPILER_FLAGS_G++_GENERAL}")
set(CMAKE_ASM_FLAGS         "${CMAKE_ASM_FLAGS} ${COMPILER_FLAGS_TARGET} ${COMPILER_FLAGS_ASM_GENERAL} ${COMPILER_FLAGS_OPT_DEBUGGING}")
set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${COMPILER_FLAGS_TARGET} ${COMPILER_FLAGS_LINKER_GENERAL}")

# The following is required to get rid of the (not supported) -rdynamic flag
# when linking the final binary.
set(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS   "" )
set(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS "" )

coloredMessage(BoldGreen "GCC Compiler Options:" STATUS)
coloredMessage(BoldWhite "${CMAKE_C_FLAGS}" STATUS)
coloredMessage(BoldGreen "G++ Compiler Options:" STATUS)
coloredMessage(BoldWhite "${CMAKE_CXX_FLAGS}" STATUS)
coloredMessage(BoldGreen "G++ Linker Options:" STATUS)
coloredMessage(BoldWhite "${CMAKE_EXE_LINKER_FLAGS}" STATUS)
